--Display Oracle version (one method).
--2. Display Oracle version (another method).
--3. Display current user.
--4. Display current day/time (formatted, and displaying AM/PM).
--5. Display your privileges.
--6. Display all user tables.
--7. Display structure for each table.
--8. List the customer number, last name, first name, and e-mail of every customer.
--9. Same query as above, include street, city, state, and sort by state in descending order, and last
name in ascending order.
--10. What is the full name of customer number 3? Display last name first.
select cus_lname. cus_fname
from customer 

--11. Find the customer number, last name, first name, and current balance for every customer whose
balance exceeds $1,000, sorted by largest to smallest balances.


--12. List the name of every commodity, and its price (formatted to two decimal places, displaying $
sign), sorted by smallest to largest price.
select com_name, to_char(com_price,'L99,999.99') as price_formatted
from commodity;
order by  com_price;

--13. Display all customers’ first and last names, streets, cities, states, and zip codes as follows (ordered by zip code descending).
--NAME ADDRESS
Victors, Sally 534 Holler Way, Charleston, WV 78345
Taylor, Stephen 456 Elm St., St. Louis, MO 57252
Davis, Beverly 123 Main St., Detroit, MI 48252
Carter, Donna 789 Peach Ave., Los Angeles, CA 48252
Silverman, Robert 857 Wilbur Rd., Phoenix, AZ 25278 
select(cus_fname)

--14. List all orders not including cereal--use subquery to find commodity id for cereal.


--15. List the customer number, last name, first name, and balance for every customer whose balance is between $500 and $1,000, (format
currency to two decimal places, displaying $ sign).

--16. List the customer number, last name, first name, and balance for every customer whose balance is greater than the average balance,
(format currency to two decimal places, displaying $ sign).

--17. List the customer number, name, and *total* order amount for each customer sorted in descending *total* order amount, (format
currency to two decimal places, displaying $ sign), and include an alias “total orders” for the derived attribute.

--18. List the customer number, last name, first name, and complete address of every customer who lives on a street with "Peach"
anywhere in the street name.

--19. List the customer number, name, and *total* order amount for each customer whose *total* order amount is greater than $1500, for
each customer sorted in descending *total* order amount, (format currency to two decimal places, displaying $ sign), and include an
alias “total orders” for the derived attribute.

--20. List the customer number, name, and number of units ordered for orders with 30, 40, or 50 units ordered.

--21. Using EXISTS operator: List customer number, name, number of orders, minimum, maximum, and sum of their order total cost, only if
there are 5 or more customers in the customer table, (format currency to two decimal places, displaying $ sign).

--22. Find aggregate values for customers:
(Note, difference between count(*) and count(cus_balance), one customer does not have a balance.)
23. Find the number of unique customers who have orders.
24. List the customer number, name, commodity name, order number, and order amount for each customer order, sorted in descending
order amount, (format currency to two decimal places, displaying $ sign), and include an alias “order amount” for the derived
attribute.
25. Modify prices for DVD players to $99.
Note: First, *be sure* to SET DEFINE OFF (don't use a semi-colon on the end).