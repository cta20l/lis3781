> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Christian T. Ash

### Assignment 3 Requirements:

*3 Parts:*

1. To run Oracle Server, download, install, and operate Microsoft Remote Desktop
2. Create tables for Customers, Commodities, and Orders
3. Queries that retrieve specific information should be created and executed

## Screenshots of the Populated tables (Oracle environment):

### Customer Table
* Customer Input and Insert

![Customer  Table 1](img/Customer_Table_Input_and_Insert.png)

* Customer Output

![Customer  Table 2](img/Customer_Table_Output.png)


#### Commodity  Table
* Commodity Input and Insert

![Commodity  Table 1](img/Commodity_Table_Input_and_Insert.png)

* Commodity Output

![Commodity  Table 2](img/Commodity_Table_Output.png)

#### Order  Table
* Order Input and Insert

![Order Table 1](img/Order_Table_Input_and_Insert.png)

* Customer Output

![Order Table 2](img/Order_Table_Output.png)


## SQL Code Query
* Question 1 - 12
![Query Code 1](img/User_Query_Statement.png)

* Question 13 - 21
![Query Code 2](img/User_Query_Statement2.png)

* Question 22 - 25
![Query Code 3](img/User_Query_Statement3.png)

#### Repo Links:

*Bitbucket Repo - Station Locations:* 

[Bitbucket Repo Link](https://bitbucket.org/ctawork/lis3781/src "Bitbucket Repo Link")

