set ANSI_WARNINGS on;
go

use master;
go

IF EXISTS (SELECT NAME FROM master.dbo.sysdatabases WHERE name = 'cta20l')
drop database cta20l;
go

IF NOT EXISTS (SELECT NAME FROM master.dbo.sysdatabases WHERE name = 'cta20l')
create database cta20l;
go

use cta20l;


-- table person
if OBJECT_ID (N'dbo.person', N'U') is not null
drop table dbo.person;
go

create table dbo.person (
    per_id smallint not null identity(1,1) primary key,
    per_ssn binary(64) null,
    per_fname varchar(20) not null,
    per_lname varchar(35) not null,
    per_gender char(1) not null check (per_gender in('m', 'f', 'o')),
    per_dob date not null,
    per_street varchar(30) not null,
    per_city varchar(30) not null,
    per_state char(2) not null default 'FL',
    per_zip int not null check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    per_email varchar(100) null,
    per_type char(1) not null check (per_type in('c', 's')),
    per_notes varchar(255) null,
    
    constraint ux_per_ssn unique nonclustered (per_ssn asc)
);


-- table phone
if OBJECT_ID (N'dbo.phone', N'U') is not null
drop table dbo.phone;
go

create table dbo.phone (
    phn_id smallint not null identity(1,1) primary key,
    per_id smallint not null,
    phn_num bigint not null check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    phn_type char(1) not null check (phn_type in('h', 'c', 'w', 'f')), 
    phn_notes varchar(255) null,
    
    constraint fk_phone_person
        foreign key (per_id)
        references dbo.person (per_id)
        on delete cascade
        on update cascade
);


-- table customer
if OBJECT_ID (N'dbo.customer', N'U') is not null
drop table dbo.customer;
go

create table dbo.customer (
    per_id smallint not null primary key,
    cus_balance decimal(7,2) not null check (cus_balance >= 0),
    cus_total_sales decimal(7,2) not null check (cus_total_sales >= 0),
    cus_notes varchar(255) null,
    
    constraint fk_customer_person
        foreign key (per_id)
        references dbo.person (per_id)
        on delete cascade
        on update cascade
);


-- table slsrep
if OBJECT_ID (N'dbo.slsrep', N'U') is not null
drop table dbo.slsrep;
go

create table dbo.slsrep (
    per_id smallint not null primary key,
    srp_yr_sales_goal decimal(8,2) not null check (srp_yr_sales_goal >= 0),
    srp_ytd_sales decimal(8,2) not null check (srp_ytd_sales >= 0),
    srp_ytd_comm decimal(7,2) not null check (srp_ytd_comm >= 0),
    srp_notes varchar(255) null,
    
    constraint fk_slsrep_person
        foreign key (per_id)
        references dbo.person (per_id)
        on delete cascade
        on update cascade
);


-- table srp_hist
if OBJECT_ID (N'dbo.srp_hist', N'U') is not null
drop table dbo.srp_hist;
go

create table dbo.srp_hist (
    sht_id smallint not null identity(1,1) primary key,
    per_id smallint not null,
    sht_type char(1) not null check (sht_type in('i', 'u', 'd')),
    sht_modified datetime not null,
    sht_modifier varchar(45) not null default system_user,
    sht_date date not null default getDate(),
    sht_yr_sales_goal decimal(8,2) not null check (sht_yr_sales_goal >= 0),
    sht_yr_total_sales decimal(8,2) not null check (sht_yr_total_sales >= 0),
    sht_yr_total_comm decimal(7,2) not null check (sht_yr_total_comm >= 0),
    sht_notes varchar(45) null,
    
    constraint fk_srp_hist_slsrep
        foreign key (per_id)
        references dbo.slsrep (per_id)
        on delete cascade
        on update cascade
);


-- table contact
if OBJECT_ID (N'dbo.contact', N'U') is not null
drop table dbo.contact;
go

create table dbo.contact (
    cnt_id int not null identity(1,1) primary key,
    per_cid smallint not null,
    per_sid smallint not null,
    cnt_date datetime not null,
    cnt_notes varchar(255) null,
    
    constraint fk_contact_customer
        foreign key (per_cid)
        references dbo.customer (per_id)
        on delete cascade
        on update cascade,
        
    constraint fk_contact_slsrep
        foreign key (per_sid)
        references dbo.slsrep (per_id)
        on delete no action
        on update no action
        -- cannot have multiple cascading paths on contact because cnt_id cannot be deleted twice in the same record
);


-- table [order]
if OBJECT_ID (N'dbo.[order]', N'U') is not null
drop table dbo.[order];
go

create table dbo.[order] (
    ord_id int not null identity(1,1) primary key,
    cnt_id int not null,
    ord_placed_date datetime not null,
    ord_filled_date datetime null,
    
    constraint fk_order_contact
        foreign key (cnt_id)
        references dbo.contact (cnt_id)
        on delete cascade
        on update cascade
);


-- table store
if OBJECT_ID (N'dbo.store', N'U') is not null
drop table dbo.store;
go

create table dbo.store (
    str_id smallint not null identity(1,1) primary key,
    str_name varchar(45) not null,
    str_street varchar(30) not null,
    str_city varchar(30) not null,
    str_state char(2) not null default 'FL',
    str_zip int not null check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_phone bigint not null check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_email varchar(100) not null,
    str_url varchar(100) not null,
    str_notes varchar(255) null
);


-- table invoice 
if OBJECT_ID (N'dbo.invoice', N'U') is not null
drop table dbo.invoice;
go

create table dbo.invoice (
    inv_id int not null identity(1,1) primary key,
    ord_id int not null,
    str_id smallint not null,
    inv_date datetime not null,
    inv_total decimal(8,2) not null check (inv_total >= 0),
    inv_paid bit not null,
    inv_notes varchar(255) null,
    
    constraint ux_ord_id unique nonclustered (ord_id asc),
    
    constraint fk_invoice_order
        foreign key (ord_id)
        references dbo.[order] (ord_id)
        on delete cascade
        on update cascade,
        
    constraint fk_invoice_store
        foreign key (str_id)
        references dbo.store (str_id)
        on delete cascade
        on update cascade
);


-- table payment
if OBJECT_ID (N'dbo.payment', N'U') is not null
drop table dbo.payment;
go

create table dbo.payment (
    pay_id int not null identity(1,1) primary key,
    inv_id int not null,
    pay_date datetime not null,
    pay_amt decimal(7,2) not null check (pay_amt >= 0),
    pay_notes varchar(255) null,
    
    constraint fk_payment_invoice
        foreign key (inv_id)
        references dbo.invoice (inv_id)
        on delete cascade
        on update cascade
);


-- table vendor
if OBJECT_ID (N'dbo.vendor', N'U') is not null
drop table dbo.vendor;
go

create table dbo.vendor (
    ven_id smallint not null identity(1,1) primary key,
    ven_name varchar(45) not null,
    ven_street varchar(30) not null,
    ven_city varchar(30) not null,
    ven_state char(2) not null default 'FL',
    ven_zip int not null check (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_phone bigint not null check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_email varchar(100) null,
    ven_url varchar(100) null,
    ven_notes varchar(255) null
);


-- table product
if OBJECT_ID (N'dbo.product', N'U') is not null
drop table dbo.product;
go

create table dbo.product (
    pro_id smallint not null identity(1,1) primary key,
    ven_id smallint not null,
    pro_name varchar(30) not null,
    pro_descript varchar(255) null,
    pro_weight float not null check (pro_weight >= 0),
    pro_qoh smallint not null check (pro_qoh >= 0),
    pro_cost decimal(7,2) not null check (pro_cost >= 0),
    pro_price decimal(7,2) not null check (pro_price >= 0),
    pro_discount decimal(3,0) null,
    pro_notes varchar(255) null,
    
    constraint fk_product_vendor
        foreign key (ven_id)
        references dbo.vendor (ven_id)
        on delete cascade
        on update cascade
);


-- table product_hist
if OBJECT_ID (N'dbo.product_hist', N'U') is not null
drop table dbo.product_hist;
go

create table dbo.product_hist (
    pht_id int not null identity(1,1) primary key,
    pro_id smallint not null,
    pht_date datetime not null,
    pht_cost decimal(7,2) not null check (pht_cost >= 0),
    pht_price decimal(7,2) not null check (pht_price >= 0),
    pht_discount decimal(3,0) null,
    pht_notes varchar(255) null,
    
    constraint fk_product_hist_product
        foreign key (pro_id)
        references dbo.product (pro_id)
        on delete cascade
        on update cascade
);


-- table order_line
if OBJECT_ID (N'dbo.order_line', N'U') is not null
drop table dbo.order_line;
go

create table dbo.order_line (
    oln_id int not null identity(1,1) primary key,
    ord_id int not null,
    pro_id smallint not null,
    oln_qty smallint not null check (oln_qty >= 0),
    oln_price decimal(7,2) not null check (oln_price >= 0),
    oln_notes varchar(255) null,
    
    constraint fk_order_line_order
        foreign key (ord_id)
        references dbo.[order] (ord_id)
        on delete cascade
        on update cascade,
        
    constraint fk_order_line_product
        foreign key (pro_id)
        references dbo.product (pro_id)
        on delete cascade
        on update cascade
);

SELECT * FROM information_schema.tables;




-- data
insert into dbo.person
(per_ssn, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_zip, per_email, per_type)
values
(HASHBYTES('SHA2_512', '123456789'), 'Michael', 'Proeber', 'm', '2001-09-03', '123 Main', 'Tallahassee', '323040000', 'michaelproeber@gmail.com', 'c'),
(HASHBYTES('SHA2_512', '234567890'), 'Carey', 'Carpenter', 'm', '1970-01-01', '234 Street', 'Tallahassee', '323040000', 'careycarpenter@gmail.com', 'c'),
(HASHBYTES('SHA2_512', '345678901'), 'Mark', 'Jowett', 'm', '1960-01-01', '345 Road', 'Tallahassee', '323040000', 'mjowett@fsu.edu', 'c'),
(HASHBYTES('SHA2_512', '456789012'), 'Sarah', 'Huerta', 'f', '1999-01-01', '456 Way', 'Tallahassee', '323040000', 'shuerta@fsu.edu', 'c'),
(HASHBYTES('SHA2_512', '567890123'), 'Xan', 'Fenton', 'o', '2008-01-01', '567 Lane', 'Ocala', '344760000', 'xfenton@gmail.com', 'c'),
(HASHBYTES('SHA2_512', '678901234'), 'Tim', 'Steis', 'm', '1990-01-01', '678 Avenue', 'Tallahassee', '323040000', 'tsteis@gmail.com', 's'),
(HASHBYTES('SHA2_512', '789012345'), 'Sheila', 'Steis', 'f', '1989-01-01', '789 Circle', 'Tampa', '111110000', 'sheilasteis@gmail.com', 's'),
(HASHBYTES('SHA2_512', '890123456'), 'Carolyn', 'Blakeslee', 'f', '1955-01-01', '890 Drive', 'Ocala', '344760000', 'carolynblakeslee@gmail.com', 's'),
(HASHBYTES('SHA2_512', '901234567'), 'Dexter', 'Steis', 'm', '1970-01-01', '901 Street', 'Orlando', '123450000', 'dsteis@gmail.com', 's'),
(HASHBYTES('SHA2_512', '012345678'), 'Alex', 'Steis', 'm', '1970-01-01', '101 Street', 'Orlando', '123450000', 'asteis@gmail.com', 's');

select * from dbo.person;


insert into dbo.phone
(per_id, phn_num, phn_type)
values
(1, 5554443333, 'h'),
(2, 4443332222, 'c'),
(3, 3332221111, 'c'),
(4, 2221110000, 'c'),
(5, 1110009999, 'c');

select * from dbo.phone;


insert into dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm)
values
(6, 100000, 60000, 2000),
(7, 80000, 35000, 3500),
(8, 150000, 84000, 9650),
(9, 125000, 87000, 15300),
(10, 98000, 43000, 8750);

select * from dbo.slsrep;


insert into dbo.customer
(per_id, cus_balance, cus_total_sales)
values
(1, 120, 12345),
(2, 12.34, 234.56),
(3, 0, 1234),
(4, 500, 600),
(5, 505, 505);

select * from dbo.customer;


insert into dbo.contact
(per_cid, per_sid, cnt_date)
values
(1, 6, '2000-01-01'),
(2, 7, '2001-01-01'),
(3, 8, '2002-01-01'),
(4, 9, '2003-01-01'),
(5, 10, '2004-01-01');

select * from dbo.contact;


insert into dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date)
values
(1, '2001-01-01', '2001-01-02'),
(2, '2002-01-01', '2002-01-02'),
(3, '2003-01-01', '2003-01-02'),
(4, '2004-01-01', '2004-01-02'),
(5, '2005-01-01', '2005-01-02');

select * from dbo.[order];


insert into dbo.store
(str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url)
values
('Walgreens', '123 Road', 'Tallahassee', 'FL', '323040000', '1112223333', 'walgreens@walgreens.com', 'walgreens.com'),
('Walmart', '234 Lane', 'Tallahassee', 'FL', '323040000', '2223334444', 'walmart@walmart.com', 'walmart.com'),
('Publix', '345 Avenue', 'Tallahassee', 'FL', '323040000', '3334445555', 'publix@publix.com', 'publix.com'),
('CVS', '456 Street', 'Tallahassee', 'FL', '323040000', '4445556666', 'cvs@cvs.com', 'cvs.com'),
('Aldi', '567 Parkway', 'Tallahassee', 'FL', '323040000', '5556667777', 'aldi@aldi.com', 'aldi.com');

select * from dbo.store;


insert into dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid)
values
(1, 1, '2001-01-01', 12.34, 1),
(2, 2, '2002-01-01', 23.45, 1),
(3, 3, '2003-01-01', 34.56, 1),
(4, 4, '2004-01-01', 45.67, 1),
(5, 5, '2005-01-01', 56.78, 1);

select * from dbo.invoice;


insert into dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url)
values
('Sysco', '123 Run', 'Tallahassee', 'FL', '323040000', '5554443333', 'sysco@sysco.com', 'sysco.com'),
('General Electric', '234 Street Road', 'Tallahassee', 'FL', '444333222', '2223334444', 'ge@ge.com', 'ge.com'),
('Cisco', '345 Avenue', 'Tallahassee', 'FL', '323040000', '3332221111', 'cisco@cisco.com', 'cisco.com'),
('Goodyear', '456 Place', 'Tallahassee', 'FL', '323040000', '2221110000', 'goodyear@goodyear.com', 'goodyear.com'),
('Chewy', '567 Place', 'Tallahassee', 'FL', '323040000', '1110009999', 'chewy@chewy.com', 'chewy.com');

select * from dbo.vendor;


insert into dbo.product
(ven_id, pro_name, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount)
values
(1, 'hammer', 2, 5, 1, 2, 0.50),
(2, 'sickle', 5, 4, 2, 3, 0.50),
(3, 'rake', 5, 3, 3, 4, 0.50),
(4, 'hoe', 5, 2, 4, 5, 0.50),
(5, 'wheelbarrow', 20, 1, 20, 25, 0.50);

select * from dbo.product;


insert into dbo.order_line
(ord_id, pro_id, oln_qty, oln_price)
values
(1, 1, 1, 10),
(2, 2, 2, 20),
(3, 3, 3, 30),
(4, 4, 4, 40),
(5, 5, 5, 50);

select * from dbo.order_line;


insert into dbo.payment
(inv_id, pay_date, pay_amt)
values
(1, '2001-01-01', 1),
(2, '2002-01-01', 2),
(3, '2003-01-01', 3),
(4, '2004-01-01', 4),
(5, '2005-01-01', 5);

select * from dbo.payment;


insert into dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount)
values
(1, '1991-01-01', 1, 10, 1),
(2, '1992-01-01', 2, 20, 2),
(3, '1993-01-01', 3, 30, 3),
(4, '1994-01-01', 4, 40, 4),
(5, '1995-01-01', 5, 50, 5); select * from dbo.product_hist;


insert into dbo.srp_hist
(per_id, sht_type, sht_modified, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm)
values
(6, 'i', getDate(), getDate(), 100000, 101000, 1000),
(7, 'i', getdate(), getdate(), 200000, 202000, 2000),
(8, 'i', getdate(), getdate(), 300000, 303000, 3000),
(9, 'i', getdate(), getdate(), 400000, 404000, 4000),
(10, 'i', getdate(), getdate(), 500000, 505000, 5000);

select * from dbo.srp_hist;