> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Christian T. Ash

### Assignment 4 Requirements:

* Creating large-scale databases using MS SQL Server

* Using the Salt and SHA 512 methods.

* On any given day, each customer has at least one sales rep (because it is a high-volume business). 

* A customer places at least one order placed by only one customer, one order line. 

* Each product may be on a number of order lines. Though, each order line contains exactly one product id.

* Every order is invoiced on one invoice, and every invoice is a bill for just one order. 

* Payments can be full or partial. They all go to the same invoice.

* A store has many invoices, but each invoice is associated with only one store. 

* One vendor provides many products, but each is provided by only one vendor.

* Must track history of products, including: cost, price, and discount percentage (if any).

* Preparation of an Entity Relationship Diagram.

#### Assignment 4 Screenshots:

* ERD for Assignment 4

![A4ERD](img/A4ERD.png)

*Screenshot of Table*:

![JDK Installation Screenshot](img/jdk_install.png)


#### Repo Links:

*Bitbucket Repo - Station Locations:* 

[Bitbucket Repo Link](https://bitbucket.org/ctawork/lis3781/src "Bitbucket Repo Link")