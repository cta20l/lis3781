> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Christian T. Ash

### Assignment 1 Requirements:

*Five Parts:*

1. Distributed Version Control with Git and Bitbucket
2. AMPPS Installation
3. Questions
4. Entity Relationship Diagram, and SQL Code (optional)
5. Bitbucket repo links:
    a. this assignment and
    b. the completed tutorial (bitbucketstationlocations)


## A1  Database Rules:
The human resource (HR) department of the ACME company wants to contract a database modeler/designer to collect the following employee data for tax purposes: job description, length of employment, benefits, number of dependents and their relationships, DOB of both the employee and anyrespective dependents. In addition, employees’ histories must be tracked. Also, include the following business rules: 

* Each employee may have one or more dependents. 
* Each employee has only one job.  
* Each job can be held by many employees. 
* Many employees may receive many benefits. 
* Many benefits may be selected by many employees (though, while they may not select any benefits—any dependents of employees may be on an employee’s plan). 

#### README.md file should include the following items:

* Screenshot of A1 ERD
* Ex1. SQL Solution
* git commands w/short descriptions

>
> #### Git commands w/short descriptions:

1. git init - Create a new local repository
2. git status - List the files you've changed and those you still need to add or commit:
3. git add -  Add one or more files to staging (index)
4. git commit - Commit changes to head (but not yet to the remote repository)
5. git push - Send changes to the master branch of your remote repository:
6. git pull -Fetch and merge changes on the remote server to your working directory
7. git fetch origin - Instead, to drop all your local changes and commits, fetch the latest history from the server and point your local master branch at it.

#### Assignment Screenshots:

#### Screenshots of A1 ERD:

![A1 ERD](img/a1.png "A1 ERD Screenshot")

#### Screenshots of A1 EX:

![A1 ERD](img/a1part1.png "A1 EX Screenshot")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:* 

[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/ctawork/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

