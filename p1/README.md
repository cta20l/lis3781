> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 -Advance Database Management

## Christian T. Ash

### LIS3781 Project 1  Requirements:

1. Create a local database of cases for city courts using the MySQL server.
2. Security of sensitive information, such as SSN's.
3. Salting and hashing the data based on stored procedures.

## Asssignment Examples:

### Entity Relational Diagram:

![P1 MWB File](img/PicModel.png "P1 ERD in .mwb format") 

### Person Data Table:

![P1 MWB File](img/PicTable.png "P1 ERD in .mwb format") 

### Links to p1.mwb and p1_solution files: 

[P1 MWB File](docs/p1.mwb "P1 ERD in .mwb format") 
 
[P1 SQL File](docs/p1.sql "P1 SQL Script")


#### Repo Links:

*Bitbucket Repo - Station Locations:* 

[Bitbucket Repo Link](https://bitbucket.org/ctawork/lis3781/src "Bitbucket Repo Link")
