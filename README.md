> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 -Advance Database Management

## Christian T. Ash

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Creating tables, database, and insert statements using MySQL Server.
    - Granting Privilages to different users on server.
    - Populating data and pushing to both local and cloud server.
    
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Download, Install, and operate on RemoteLabs Oracle Server.
    - Create the tables: Customer, Commodity, and Order
    - Create and Excute Queries to recieve the Data,

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create a local database of cases for city courts using the MySQL server.
    - Security of sensitive information, such as SSN's.
    - Salting and hashing the data based on stored procedures.

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Creating large-scale databases using MS SQL Server
    - Using the Salt and SHA 512 methods.
    - Preparation of an Entity Relationship Diagram.


5. [A5 README.md](a5/README.md "My A5 README.md file")
    - High-volume databases are created using MS SQL Server
    - Utilizing Salt and the SHA512 algorithm.
    - Entity Relationship Diagrams are part of the deliverables.

37. [P2 README.md](p2/README.md "My P2 README.md file")
    - Installation and import of MongoDB
    - A brief introduction to NoSQL databases
    - Reports that must be generated and JSON code solution
