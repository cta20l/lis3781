> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS3781

## Christian T. Ash

### Assignment 2 Requirements:

* Create tables, database, and insert statements using MySQL Server.
* Granting Privileges to different users on server.
* Populating data and pushing to both local and cloud server.

#### MySQL Query Screenshots:

##### Table Inserts:

* Table 1: Commany
* ![ Screenshot Company](img/Screenshot1.png)
* Table 2: Customer
* ![ Screenshot Customer](img/Screenshot2.png)
 

#### Table with Poplation Table:
* ![Screenshot Poplation Table](img/Screenshot3.png)


## Grant Privileges:

### Grant User 1  Privilege:

 ![ Screenshot Poplation Table](img/Screenshot4.png)

### Grant User 2  Privilege:

 ![Drant 2](img/Screenshot5.png)

### Local Servers:

  ![Local Servers](img/Screenshot6.png)

## Login as User 1:

### Screenshot of User 1:
![ Screenshot User 1](img/Screenshot7.png)

##  Login as User 2:

### Screenshot of User 2:
![ Screenshot User 2](img/Screenshot8.png)

### Errors for User 1 Company:
![Screenshot Erroes](img/Screenshot12.png)

### Errors for User 21 Customer:
![Screenshot Errors](img/Screenshot13.png)

### Errors for User 2 Company:
![Screenshot Erroes](img/Screenshot10.png)

### Errors for User 2 Customer:
![Screenshot Errors](img/Screenshot11.png)

## Screenshot of Drop Tables in Admin:

![Screenshot Drop table](img/Screenshot9.png)

#### Repo Links:

*Bitbucket Repo Link  - Station Locations:*
[Bitbucket Repo Link](https://bitbucket.org/ctawork/lis3781/src/master/ "Bitbucket Repo Link")
