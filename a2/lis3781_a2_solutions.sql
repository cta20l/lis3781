DROP database if exists cta20l;
create database if not exists cta20l;

use cta20l;

CREATE TABLE IF NOT EXISTS company
(
cmp_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership'),
cmp_street VARCHAR(30) NOT NULL,
cmp_city VARCHAR(30) NOT NULL,
cmp_state CHAR(2) NOT NULL,
cmp_zip int(9) unsigned ZEROFILL NOT NULL COMMENT 'no dashes',
cmp_phone bigint unsigned NOT NULL COMMENT 'ssn and zip codes can be zero-filled, but not US area codes',
cmp_ytd_sales DECIMAL(10,2) unsigned NOT NULL COMMENT '12,345,678.90',
cmp_email VARCHAR(100) NULL,
cmp_url VARCHAR(100) NULL,
cmp_notes VARCHAR(255) NULL,
PRIMARY KEY (cmp_id)

)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

INSERT INTO company
VALUES
(null,'LLC','4110 Old Redmond Rd.','Redmond','WA','000029021','2065558122','678345.00',null,'http://www.theexample1.com','company notes1'),
(null,'C-Corp','507 = 20th Ave. E. Apt. 2A','Seattle','WA','381226749','2065559857','12345678.00',null,'http://www.theexample2.com','company notes2'),
(null,'S-Corp','908 W. Capital Way','Tacoma','WA','004011298','25559482','9945678.00',null,'http://www.theexample3.com','company notes3'),
(null,'Non-Profit-Corp','722 Moss Bay Blvd.','Kirkland','WA','127337845','2065553412','1345678.00',null,'http://www.theexample4.com','company notes4'),
(null,'Partnership','493 Hope Route','Tampa','FL','983368745','5969295863','156932.00',null,'http://www.theexample5.com','company notes5');

SHOW WARNINGS;

select * from company;

DROP TABLE IF EXISTS customer;
CREATE TABLE IF NOT EXISTS customer
	(
		cus_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
		cmp_id INT UNSIGNED NOT NULL,
		cus_ssn binary(64) NOT NULL,
		cus_salt binary(64) NOT NULL,
		cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),
		cus_first VARCHAR(15) NOT NULL,
		cus_last VARCHAR(30) NOT NULL,
		cus_street VARCHAR(30) NOT NULL,
		cus_city VARCHAR(30) NOT NULL,
		cus_state CHAR(2) NOT NULL,
cus_zip int(9) unsigned ZEROFILL NULL,
cus_phone bigint unsigned zerofill NULL COMMENT 'ssn and zip codes can be zero-filled, but not US area codes',
cus_email VARCHAR(100) NULL,
cus_balance DECIMAL(7,2) unsigned NULL COMMENT '12,345.67',
cus_tot_sales DECIMAL(7,2) unsigned NULL,
cus_notes VARCHAR(255) NULL,
PRIMARY KEY (cus_id),

UNIQUE INDEX ux_cus_ssn (cus_ssn ASC),
INDEX idx_cmp_id (cmp_id ASC),

CONSTRAINT fk_customer_company
FOREIGN KEY (cmp_id)
REFERENCES company (cmp_id)
ON DELETE NO ACTION
ON UPDATE CASCADE
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

set @salt=RANDOM_BYTES(64);

INSERT INTO customer (cus_id, cmp_id,cus_ssn,cus_salt, cus_type, cus_first, cus_last, cus_street, cus_city,cus_state,cus_zip,cus_email,cus_balance,cus_tot_sales,cus_notes)
	VALUES
	(null,4,unhex(SHA2(CONCAT(@salt, 001456789),512)),@salt,'Loyal','Mario','Casis','891 Drift Dr.','Stanton','TX',2145559482,'test2@mymail.com',675.57,87341.00,null),
	(null,3,unhex(SHA2(CONCAT(@salt, 002456789),512)),@salt,'Impulse','Valerie','Lieblong','421 Calamari Vista','Atlanta','GA', 2145553412,'test3@mymail.com',8730.23,92678.00,'customer notes3'),
	(null,5,unhex(SHA2(CONCAT(@salt, 003456789),512)),@salt,'Need-Based','Peach','Jeffries','915 Drive Past','Los Angeles','CA',2145558122,'test4@mymail.com',2651.19,78345.00,null),
	(null,1,unhex(SHA2(CONCAT(@salt, 004456789),512)),@salt,'Wandering','Techo','Rogers','329 Volume Ave.','Chicago','IL',2145551189,'testS5@mymail.com',782.73,23471.00,'customer notes5'),

	(null,2,unhex(SHA2(CONCAT(@salt, 123456789),512)),@salt,'Discount','Wilbur','Denaway','23 Billings Gate','El Paso','TX',336261234,'test1@mymail.com',8391.87,3742.00,'customer notes1');
SHOW WARNINGS;
select * from company;
select * from customer;

describe customer;
