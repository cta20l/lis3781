-- LIS3781 Assignment A5
set ANSI_WARNINGS on;
go

use master;
go

IF EXISTS (SELECT NAME FROM master.dbo.sysdatabases WHERE name = 'cta20l')
drop database cta20l;
go

IF NOT EXISTS (SELECT NAME FROM master.dbo.sysdatabases WHERE name = 'cta20l')
create database cta20l;
go

use cta20l;

-- ----------------------------
-- table person
-- ----------------------------
if OBJECT_ID (N'dbo.person', N'U') is not null
drop table dbo.person;
go

create table dbo.person (
    per_id smallint not null identity(1,1) primary key,
    per_ssn binary(64) null,
    per_fname varchar(20) not null,
    per_lname varchar(35) not null,
    per_gender char(1) not null check (per_gender in('m', 'f', 'o')),
    per_dob date not null,
    per_street varchar(30) not null,
    per_city varchar(30) not null,
    per_state char(2) not null default 'FL',
    per_zip int not null check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    per_email varchar(100) null,
    per_type char(1) not null check (per_type in('c', 's')),
    per_notes varchar(255) null,
    
    constraint ux_per_ssn unique nonclustered (per_ssn asc)
);

-- ----------------------------
-- table phone
-- ----------------------------
if OBJECT_ID (N'dbo.phone', N'U') is not null
drop table dbo.phone;
go

create table dbo.phone (
    phn_id smallint not null identity(1,1) primary key,
    per_id smallint not null,
    phn_num bigint not null check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    phn_type char(1) not null check (phn_type in('h', 'c', 'w', 'f')), 
    phn_notes varchar(255) null,
    
    constraint fk_phone_person
        foreign key (per_id)
        references dbo.person (per_id)
        on delete cascade
        on update cascade
);

-- ----------------------------
-- table customer
-- ----------------------------
if OBJECT_ID (N'dbo.customer', N'U') is not null
drop table dbo.customer;
go

create table dbo.customer (
    per_id smallint not null primary key,
    cus_balance decimal(7,2) not null check (cus_balance >= 0),
    cus_total_sales decimal(7,2) not null check (cus_total_sales >= 0),
    cus_notes varchar(255) null,
    
    constraint fk_customer_person
        foreign key (per_id)
        references dbo.person (per_id)
        on delete cascade
        on update cascade
);

-- ----------------------------
-- table slsrep
-- ----------------------------
if OBJECT_ID (N'dbo.slsrep', N'U') is not null
drop table dbo.slsrep;
go

create table dbo.slsrep (
    per_id smallint not null primary key,
    srp_yr_sales_goal decimal(8,2) not null check (srp_yr_sales_goal >= 0),
    srp_ytd_sales decimal(8,2) not null check (srp_ytd_sales >= 0),
    srp_ytd_comm decimal(7,2) not null check (srp_ytd_comm >= 0),
    srp_notes varchar(255) null,
    
    constraint fk_slsrep_person
        foreign key (per_id)
        references dbo.person (per_id)
        on delete cascade
        on update cascade
);

-- ----------------------------
-- table srp_hist
-- ----------------------------
if OBJECT_ID (N'dbo.srp_hist', N'U') is not null
drop table dbo.srp_hist;
go

create table dbo.srp_hist (
    sht_id smallint not null identity(1,1) primary key,
    per_id smallint not null,
    sht_type char(1) not null check (sht_type in('i', 'u', 'd')),
    sht_modified datetime not null,
    sht_modifier varchar(45) not null default system_user,
    sht_date date not null default getDate(),
    sht_yr_sales_goal decimal(8,2) not null check (sht_yr_sales_goal >= 0),
    sht_yr_total_sales decimal(8,2) not null check (sht_yr_total_sales >= 0),
    sht_yr_total_comm decimal(7,2) not null check (sht_yr_total_comm >= 0),
    sht_notes varchar(45) null,
    
    constraint fk_srp_hist_slsrep
        foreign key (per_id)
        references dbo.slsrep (per_id)
        on delete cascade
        on update cascade
);

-- ----------------------------
-- table contact
-- ----------------------------
if OBJECT_ID (N'dbo.contact', N'U') is not null
drop table dbo.contact;
go

create table dbo.contact (
    cnt_id int not null identity(1,1) primary key,
    per_cid smallint not null,
    per_sid smallint not null,
    cnt_date datetime not null,
    cnt_notes varchar(255) null,
    
    constraint fk_contact_customer
        foreign key (per_cid)
        references dbo.customer (per_id)
        on delete cascade
        on update cascade,
        
    constraint fk_contact_slsrep
        foreign key (per_sid)
        references dbo.slsrep (per_id)
        on delete no action
        on update no action
        -- cannot have multiple cascading paths on contact because cnt_id cannot be deleted twice in the same record
);

-- ----------------------------
-- table [order]
-- ----------------------------
if OBJECT_ID (N'dbo.[order]', N'U') is not null
drop table dbo.[order];
GO

create table dbo.[order]
 (
    ord_id int not null identity(1,1),
    cnt_id int not null,
    ord_placed_date datetime not null,
    ord_filled_date datetime null,
    ord_notes VARCHAR(225) null,
	PRIMARY KEY (ord_id),

    constraint fk_order_contact
        foreign key (cnt_id)
        references dbo.contact (cnt_id)
        on delete cascade
        on update cascade
);

-- table vendor
if OBJECT_ID (N'dbo.vendor', N'U') is not null
drop table dbo.vendor;
go

create table dbo.vendor (
    ven_id smallint not null identity(1,1) primary key,
    ven_name varchar(45) not null,
    ven_street varchar(30) not null,
    ven_city varchar(30) not null,
    ven_state char(2) not null default 'FL',
    ven_zip int not null check (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_phone bigint not null check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_email varchar(100) null,
    ven_url varchar(100) null,
    ven_notes varchar(255) null
);


-- table product
if OBJECT_ID (N'dbo.product', N'U') is not null
drop table dbo.product;
go

create table dbo.product (
    pro_id smallint not null identity(1,1) primary key,
    ven_id smallint not null,
    pro_name varchar(30) not null,
    pro_descript varchar(255) null,
    pro_weight float not null check (pro_weight >= 0),
    pro_qoh smallint not null check (pro_qoh >= 0),
    pro_cost decimal(7,2) not null check (pro_cost >= 0),
    pro_price decimal(7,2) not null check (pro_price >= 0),
    pro_discount decimal(3,0) null,
    pro_notes varchar(255) null,
    
    constraint fk_product_vendor
        foreign key (ven_id)
        references dbo.vendor (ven_id)
        on delete cascade
        on update cascade
);


-- table product_hist
if OBJECT_ID (N'dbo.product_hist', N'U') is not null
drop table dbo.product_hist;
go

create table dbo.product_hist (
    pht_id int not null identity(1,1) primary key,
    pro_id smallint not null,
    pht_date datetime not null,
    pht_cost decimal(7,2) not null check (pht_cost >= 0),
    pht_price decimal(7,2) not null check (pht_price >= 0),
    pht_discount decimal(3,0) null,
    pht_notes varchar(255) null,
    
    constraint fk_product_hist_product
        foreign key (pro_id)
        references dbo.product (pro_id)
        on delete cascade
        on update cascade
);


-- table order_line
if OBJECT_ID (N'dbo.order_line', N'U') is not null
drop table dbo.order_line;
go

create table dbo.order_line (
    oln_id int not null identity(1,1) primary key,
    ord_id int not null,
    pro_id smallint not null,
    oln_qty smallint not null check (oln_qty >= 0),
    oln_price decimal(7,2) not null check (oln_price >= 0),
    oln_notes varchar(255) null,
    
    constraint fk_order_line_order
        foreign key (ord_id)
        references dbo.[order] (ord_id)
        on delete cascade
        on update cascade,
        
    constraint fk_order_line_product
        foreign key (pro_id)
        references dbo.product (pro_id)
        on delete cascade
        on update cascade
);

-- table time
IF OBJECT_ID (N'dbo.time', N'U') is not null
DROP TABLE dbo.time; 
GO

CREATE TABLE dbo.time
(
    tim_id INT NOT NULL Identity(1,1),
    tim_yr SMALLINT NOT NULL, -- 2 byte Integer (no YEAR data type in MS SQL Server)
    tim_qtr TINYINT NOT NULL, -- 1.4
    tim_month TINYINT NOT NULL, -- 1 - 12
    tim_week TINYINT NOT NULL, -- 1 - 52 
    tim_day TINYINT NOT NULL, -- 1-7
    tim_time TIME NOT NULL, -- based on 24-hour clock tim_notes
    tim_notes VARCHAR(255) NULL, 
    PRIMARY KEY (tim_id)
);
GO

-- table region
IF OBJECT_ID (N'dbo.region', N'U') IS NOT NULL
 DROP TABLE dbo.region;
 GO
 
CREATE TABLE region
(
    reg_id TINYINT NOT NULL Identity(1,1),
    reg_name CHAR(1) NOT NULL, -- n,e,s,w,c (north, east, south, west, central)
    reg_notes VARCHAR(255) NULL,
    PRIMARY KEY (reg_id)
);
GO


-- table state
IF OBJECT_ID (N'dbo.state', N'U') IS NOT NULL
DROP TABLE dbo.state; 
GO

CREATE TABLE dbo.state
(
    ste_id TINYINT NOT NULL identity(1,1),
    reg_id TINYINT NOT NULL,
    ste_name CHAR(2) NOT NULL DEFAULT 'FL',
    ste_notes VARCHAR(255) NULL,
    PRIMARY KEY (ste_id),

    CONSTRAINT fk_state_region
    FOREIGN KEY (reg_id) 
    REFERENCES dbo.region (reg_id) 
    ON DELETE CASCADE
ON UPDATE CASCADE
);
GO

-- table city
IF OBJECT_ID (N'dbo.city', N'U') IS NOT NULL
DROP TABLE dbo.city;
GO

CREATE TABLE dbo.city
(
	cty_id SMALLINT NOT NULL identity (1,1) PRIMARY KEY ,
    ste_id TINYINT NOT NULL,
    cty_name VARCHAR(30) NOT NULL,
    cty_notes VARCHAR(255) NULL,
 
    constraint fk_city_state
    FOREIGN KEY (ste_id)
    REFERENCES dbo.state (ste_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO


-- table store
IF OBJECT_ID (N'dbo.store', N'U') IS NOT NULL
DROP TABLE dbo.store;
GO

CREATE TABLE dbo.store
(
    str_id SMALLINT NOT NULL identity(1,1) PRIMARY KEY,
    cty_id SMALLINT NOT NULL, 
    str_name VARCHAR(45) NOT NULL,
    str_street VARCHAR(30) NOT NULL, 
    str_city VARCHAR(35) NOT NULL,
    str_state CHAR(2) NOT NULL,
    str_zip int NOT NULL check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'), 
    str_phone bigint NOT NULL, -- check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9)'), 
    str_email VARCHAR(100) NOT NULL, 
    str_url VARCHAR(100) NOT NULL, 
    str_notes VARCHAR(255) NULL, 


    constraint fk_store_city
        foreign key (cty_id)
        references dbo.city (cty_id)
        on delete cascade
        on update cascade
);
GO


-- table invoice 
if OBJECT_ID (N'dbo.invoice', N'U') is not null
drop table dbo.invoice;
go

create table dbo.invoice (
    inv_id int not null identity(1,1) primary key,
    ord_id int not null,
    str_id smallint not null,
    inv_date datetime not null,
    inv_total decimal(8,2) not null check (inv_total >= 0),
    inv_paid bit not null,
    inv_notes varchar(255) null,
    
    constraint ux_ord_id unique nonclustered (ord_id asc),
    
    constraint fk_invoice_order
        foreign key (ord_id)
        references dbo.[order] (ord_id)
        on delete cascade
        on update cascade,
        
    constraint fk_invoice_store
        foreign key (str_id)
        references dbo.store (str_id)
        on delete cascade
        on update cascade
);


-- table payment
if OBJECT_ID (N'dbo.payment', N'U') is not null
drop table dbo.payment;
go

create table dbo.payment (
    pay_id int not null identity(1,1) primary key,
    inv_id int not null,
    pay_date datetime not null,
    pay_amt decimal(7,2) not null check (pay_amt >= 0),
    pay_notes varchar(255) null,
    
    constraint fk_payment_invoice
        foreign key (inv_id)
        references dbo.invoice (inv_id)
        on delete cascade
        on update cascade
);


-- table sale
IF OBJECT_ID (N'dbo.sale', N'U') IS NOT NULL
DROP TABLE dbo.sale;
GO

CREATE TABLE dbo.sale
(
    pro_id SMALLINT NOT NULL, 
    str_id SMALLINT NOT NULL,
    cnt_id INT NOT NULL, 
    tim_id INT NOT NULL,
    sal_qty SMALLINT NOT NULL, 
    sal_price DECIMAL(8,2) NOT NULL,
    sal_total DECIMAL(8,2) NOT NULL,
    sal_notes VARCHAR(255) NULL,
    PRIMARY KEY (pro_id, cnt_id, tim_id, str_id),
    
    -- make sure combination of time, contact, store, and product are unique 
    CONSTRAINT ux_pro_id_str_id_cnt_id_tim_id
        unique nonclustered (pro_id ASC, str_id ASC, cnt_id ASC, tim_id ASC),

    CONSTRAINT fk_sale_time
        FOREIGN KEY (tim_id)
        REFERENCES dbo.time (tim_id) 
        ON DELETE CASCADE 
        ON UPDATE CASCADE,

    CONSTRAINT fk_sale_contact
        FOREIGN KEY (cnt_id)
        REFERENCES dbo.contact (cnt_id) 
        ON DELETE CASCADE 
        ON UPDATE CASCADE,

    CONSTRAINT fk_sale_store
        FOREIGN KEY (str_id)
        REFERENCES dbo.store (str_id) 
        ON DELETE CASCADE 
        ON UPDATE CASCADE,

    CONSTRAINT fk_sale_product
        FOREIGN KEY (pro_id)
        REFERENCES dbo.product (pro_id) 
        ON DELETE CASCADE 
        ON UPDATE CASCADE,
);
GO


SELECT * FROM information_schema.tables;






-- DATA:


-- data for person
insert into dbo.person
(per_ssn, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_zip, per_email, per_type)
values
(HASHBYTES('SHA2_512', '123456789'), 'Christian', 'Ash', 'm', '1999-01-10', '123 Main', 'Tallahassee', '323040000', 'christianash@gmail.com', 'c'),
(HASHBYTES('SHA2_512', '234567890'), 'Mario', 'Bota', 'm', '1970-01-01', '234 Street', 'Tallahassee', '323040000', 'mario123@gmail.com', 'c'),
(HASHBYTES('SHA2_512', '345678901'), 'Luigi', 'Turbo', 'm', '1960-01-01', '345 Road', 'Tallahassee', '323040000', 'test1@gmail.com', 'c'),
(HASHBYTES('SHA2_512', '456789012'), 'Peach', 'Huerta', 'f', '1999-01-01', '456 Way', 'Tallahassee', '323040000', 'test2@gmail.com', 'c'),
(HASHBYTES('SHA2_512', '567890123'), 'Yan', 'Gummy', 'o', '2008-01-01', '567 Lane', 'Ocala', '344760000', 'yummy@gmail.com', 'c'),
(HASHBYTES('SHA2_512', '678901234'), 'Tim', 'Steis', 'm', '1990-01-01', '678 Avenue', 'Tallahassee', '323040000', 'tsteis@gmail.com', 's'),
(HASHBYTES('SHA2_512', '789012345'), 'Sheila', 'Steis', 'f', '1989-01-01', '789 Circle', 'Tampa', '111110000', 'sheilasteis@gmail.com', 's'),
(HASHBYTES('SHA2_512', '890123456'), 'Carolyn', 'Blakeslee', 'f', '1955-01-01', '890 Drive', 'Ocala', '344760000', 'carolynblakeslee@gmail.com', 's'),
(HASHBYTES('SHA2_512', '901234567'), 'Dexter', 'Steis', 'm', '1970-01-01', '901 Street', 'Orlando', '123450000', 'dsteis@gmail.com', 's'),
(HASHBYTES('SHA2_512', '012345678'), 'Alex', 'Steis', 'm', '1970-01-01', '101 Street', 'Orlando', '123450000', 'asteis@gmail.com', 's');

select * from dbo.person;


-- data for phone
insert into dbo.phone
(per_id, phn_num, phn_type)
values
(1, 5554443333, 'h'),
(2, 4443332222, 'c'),
(3, 3332221111, 'c'),
(4, 2221110000, 'c'),
(5, 1110009999, 'c');

select * from dbo.phone;


-- data for slsrep
insert into dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm)
values
(6, 100000, 60000, 2000),
(7, 80000, 35000, 3500),
(8, 150000, 84000, 9650),
(9, 125000, 87000, 15300),
(10, 98000, 43000, 8750);

select * from dbo.slsrep;


-- data for customer
insert into dbo.customer
(per_id, cus_balance, cus_total_sales)
values
(1, 120, 12345),
(2, 12.34, 234.56),
(3, 0, 1234),
(4, 500, 600),
(5, 505, 505);

select * from dbo.customer;


-- data for contact
insert into dbo.contact
(per_cid, per_sid, cnt_date)
values
(1, 6, '2000-01-01'),
(2, 7, '2001-01-01'),
(3, 8, '2002-01-01'),
(4, 9, '2003-01-01'),
(5, 10, '2004-01-01');

select * from dbo.contact;


-- data for [order]
insert into dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date)
values
(1, '2001-01-01', '2001-01-02'),
(2, '2002-01-01', '2002-01-02'),
(3, '2003-01-01', '2003-01-02'),
(4, '2004-01-01', '2004-01-02'),
(5, '2005-01-01', '2005-01-02');

select * from dbo.[order];


-- data for vendor
insert into dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url)
values
('Sysco', '123 Run', 'Tallahassee', 'FL', '323040000', '5554443333', 'sysco@sysco.com', 'sysco.com'),
('General Electric', '234 Street Road', 'Tallahassee', 'FL', '444333222', '2223334444', 'ge@ge.com', 'ge.com'),
('Cisco', '345 Avenue', 'Tallahassee', 'FL', '323040000', '3332221111', 'cisco@cisco.com', 'cisco.com'),
('Goodyear', '456 Place', 'Tallahassee', 'FL', '323040000', '2221110000', 'goodyear@goodyear.com', 'goodyear.com'),
('Chewy', '567 Place', 'Tallahassee', 'FL', '323040000', '1110009999', 'chewy@chewy.com', 'chewy.com');

select * from dbo.vendor;


-- data for product
insert into dbo.product
(ven_id, pro_name, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount)
values
(1, 'hammer', 2, 5, 1, 2, 0.50),
(2, 'sickle', 5, 4, 2, 3, 0.50),
(3, 'rake', 5, 3, 3, 4, 0.50),
(4, 'hoe', 5, 2, 4, 5, 0.50),
(5, 'wheelbarrow', 20, 1, 20, 25, 0.50);

select * from dbo.product;


-- data for order_line
insert into dbo.order_line
(ord_id, pro_id, oln_qty, oln_price)
values
(1, 1, 1, 10),
(2, 2, 2, 20),
(3, 3, 3, 30),
(4, 4, 4, 40),
(5, 5, 5, 50);

select * from dbo.order_line;

select * from dbo.payment;


-- data for product_hist
insert into dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount)
values
(1, '1991-01-01', 1, 10, 1),
(2, '1992-01-01', 2, 20, 2),
(3, '1993-01-01', 3, 30, 3),
(4, '1994-01-01', 4, 40, 4),
(5, '1995-01-01', 5, 50, 5); 

select * from dbo.product_hist;


-- data for srp_hist
insert into dbo.srp_hist
(per_id, sht_type, sht_modified, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm)
values
(6, 'i', getDate(), getDate(), 100000, 101000, 1000),
(7, 'i', getdate(), getdate(), 200000, 202000, 2000),
(8, 'i', getdate(), getdate(), 300000, 303000, 3000),
(9, 'i', getdate(), getdate(), 400000, 404000, 4000),
(10, 'i', getdate(), getdate(), 500000, 505000, 5000);

select * from dbo.srp_hist;


-- data for time
INSERT INTO dbo.time
(tim_yr, tim_qtr, tim_month, tim_week, tim_day, tim_time, tim_notes) 
VALUES
(2008, 2, 5, 19, 7, '11:59:59', NULL),
(2010, 4, 12, 49, 4, '08:34:21', NULL),
(1999, 4, 12, 52, 5, '05:21:34', NULL),
(2011, 3, 8, 36, 1, '09:32:18', NULL),
(2001, 3, 7, 27, 2, '23:56:32', NULL), 
(2008, 1, 1, 5, 4, '04:22:36', NULL),
(2010, 2, 4, 14, 5, '02:49:11', NULL), 
(2014, 1, 2, 8, 2, '12:27:14', NULL), 
(2013, 3, 9, 38, 4, '10:12:28', NULL),
(2012, 4, 11, 47, 3, '22:36:22', NULL), 
(2014, 2, 6, 23, 3, '19:07:10', NULL);
GO

select * from dbo.time;


-- data for region
INSERT INTO dbo.region
(reg_name, reg_notes) 
VALUES 
('c', NULL),
('n', NULL),
('e', NULL),
('s', NULL),
('w', NULL); 
GO

select * from dbo.region;


-- data for state
INSERT INTO state 
(reg_id, ste_name, ste_notes)
VALUES
(1, 'MI', NULL),
(3, 'II', NULL),
(4, 'WA', NULL),
(5, 'FL', NULL), 
(2, 'TX', NULL);
GO

select * from dbo.state;


-- data for city
INSERT INTO dbo.city
(ste_id, cty_name, cty_notes) 
VALUES
(1, 'Detroit', NULL),
(2, 'Aspen', NULL),
(2, 'Chicago', NULL), 
(3, 'Clover', NULL),
(4, 'St. Louis',NULL);
GO

select * from dbo.city;


-- data for store
insert into dbo.store
(cty_id, str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url)
values
(2, 'Walgreens', '123 Road', 'Tallahassee', 'FL', '323040000', '1112223333', 'walgreens@walgreens.com', 'walgreens.com'),
(3, 'Walmart', '234 Lane', 'Tallahassee', 'FL', '323040000', '2223334444', 'walmart@walmart.com', 'walmart.com'),
(4, 'Publix', '345 Avenue', 'Tallahassee', 'FL', '323040000', '3334445555', 'publix@publix.com', 'publix.com'),
(5, 'CVS', '456 Street', 'Tallahassee', 'FL', '323040000', '4445556666', 'cvs@cvs.com', 'cvs.com'),
(1, 'Aldi', '567 Parkway', 'Tallahassee', 'FL', '323040000', '5556667777', 'aldi@aldi.com', 'aldi.com');
GO

select * from dbo.store;


-- data for invoice
insert into dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid)
values
(1, 1, '2001-01-01', 12.34, 1),
(2, 2, '2002-01-01', 23.45, 1),
(3, 3, '2003-01-01', 34.56, 1),
(4, 4, '2004-01-01', 45.67, 1),
(5, 5, '2005-01-01', 56.78, 1);

select * from dbo.invoice;


-- data for payment
insert into dbo.payment
(inv_id, pay_date, pay_amt)
values
(1, '2001-01-01', 1),
(2, '2002-01-01', 2),
(3, '2003-01-01', 3),
(4, '2004-01-01', 4),
(5, '2005-01-01', 5);


-- data for sale
INSERT INTO dbo.sale
(pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes)
VALUES 
(1, 5, 5, 3, 20, 9.99, 199.8, NULL), 
(2, 4, 2, 2, 5, 5.99, 29.95, NULL), 
(3, 3, 4, 1, 30, 3.99, 119.7, NULL), 
(4, 2, 1, 5, 15, 18.99, 284.85, NULL),
(5, 1, 2, 4, 6, 11.99, 71.94, NULL), 
(5, 2, 5, 6, 10, 9.99, 199.8, NULL), 
(4, 3, 5, 7, 5, 5.99, 29.95, NULL),
(3, 1, 4, 8, 30, 3.99, 119.7, NULL), 
(2, 3, 1, 9, 15, 18.99, 284.85, NULL),
(1, 4, 2, 10, 6, 11.99, 71.94, NULL),
(2, 1, 1, 5, 9, 18.99, 284.85, NULL),
(5, 2, 2, 4, 8, 11.99, 71.94, NULL),
(3, 3, 3, 3, 7, 11.99, 119.9, NULL),
(2, 4, 5, 2, 6, 9.99, 199.8, NULL), 
(1, 5, 4, 1, 5, 5.99, 29.95, NULL), 
(3, 4, 3, 3, 4, 3.99, 119.7, NULL), 
(5, 3, 2, 5, 3, 18.99, 284.85, NULL),
(4, 2, 4, 7, 2, 11.99, 71.94, NULL), 
(3, 1, 5, 9, 1, 9.99, 199.8, NULL), 
(1, 2, 4, 10, 10, 5.99, 29.95, NULL),
(3, 3, 3, 9, 9, 3.99, 119.7, NULL), 
(5, 4, 2, 8, 8, 18.99, 284.85, NULL),
(4, 5, 1, 7, 7, 11.99, 71.94, NULL),
(1, 2, 2, 6, 10, 3.99, 119.7, NULL), 
(1, 2, 3, 11, 10, 11.99, 119.9, NULL);
GO

select * from dbo.sale;