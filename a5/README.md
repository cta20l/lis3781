> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Christian T. Ash

### Assignment 5 Requirements:

    - High-volume databases are created using MS SQL Server
    - Utilizing Salt and the SHA512 algorithm.
    - Entity Relationship Diagrams are part of the deliverables.

#### Assignment 5 Screenshots:

* ERD for Assignment 5

![A5ERD](img/A5ERD.png)

*Screenshot of Table*:

![Table Screenshot](img/Table1.png)
![Table Screenshot](img/Table2.png)
![Table Screenshot](img/Table3.png)
![Table Screenshot](img/Table4.png)
![Table Screenshot](img/Table5.png)
![Table Screenshot](img/Table6.png)

#### Repo Links:

*Bitbucket Repo - Station Locations:* 

[Bitbucket Repo Link](https://bitbucket.org/ctawork/lis3781/src "Bitbucket Repo Link")