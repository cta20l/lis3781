-- 1. Log in remotely using command-line--MySQL Workbench is *not* permitted. 
 use 
 
-- 2. Display user, timestamp, and MySQL version: 
select user(), now(), version();
 
-- 3. Display *your* grants: 
 show grants
 
-- 4. Display all databases. 
 show databases;
 
-- 5. Display all tables for lis3781: 
 show tables;
 
-- 6. Display structure for each table: 

 describe absence;
 
-- 7. Display data for each table: 
select * from absence;

-- Required reports: 
 
-- 1. display all president's first and last names, and birth dates, for those presidents born before Jan. 1, 1750: 
select * from president;
select last_name, first_name, birth
from president
where birth < '1750-10-01'; 

-- -- 2. display all president's first and last names, birth dates and states, for those presidents born in either Virginia or Massachusetts: 
select last_name, first_name, birth, state 
from president
where state= 'VA' or state='MA';
 
-- 3. display all president's first and last names, birth dates and states, for those presidents born before Jan. 1, 1750, and, born in either Virginia or Massachusetts: 
select last_name, first_name, birth, state 
from president
where birth < '1750-10-01'
and state= 'VA' or state='MA';
 
-- 4. display all president's first and last names, and name suffixes, only for presidents with a name suffix 
 select last_name, first_name, suffix 
 from president
 where suffix <> null;
 
-- 5. display all president's first and last names, and births, only for the five most recently born presidents 
 
select last_name, first_name, birth 
from president
order by birth desc
limit 5;
 
-- 6. display all president's first and last names, and births, only for the second set of five most recently born presidents (i.e., 6 - 10) 
select last_name, first_name, birth 
from president
order by birth desc 
limit 5, 5;
 
-- 7. display a random set of three president's first and last names, and births
select last_name, first_name, birth 
from president
order by rand() limit 3;
 
 
-- 8. display all president's first and last names as "Presidents' Names,"
-- birth cities and states as "Places of Birth," and birth and death dates,
-- for those presidents who died between 1970 and 2010, order by most recent deaths: 

select concat(first_name, ", ", last_name), as 'President Name',
concat(city, ", " state) as "Places of Birth", birth, death
from president 
where death >= '1970-01-01' and death < '2010-01-01'
order by death desc;

-- 9. display all president's first and last names, and birth dates, for those presidents born in the month of March, sort in descending order of birth date 

select last_name, first_name, birth
from president
where monthname(birth)="March"
order by death desc;



-- 10. display all president's first and last names, and death dates, for those presidents who  died the day after Christmas, sort in descending order of death date 
select last_name, first_name, death
from president
where monthname(death)='December'
 
 
-- 11. display all member's first and last names, and expiration dates, whose expiration dates are at least six months ago. 
select last_name, first_name, 
from president

 
-- NOTE: After having found this query result set, the members could be sent a reminder, or the records could be purged. 
 
-- 12. display top 10 oldest presidents who are now deceased, in descending order of age 
select last_name, first_name, birth, death
timestampdiff(year,birth,death) as age
from president
where death is not pull
order by age desc limit 10;
 
-- 13. display all president's first and last names, birth and death dates, and ages, whose names begin with "w," order by last name:  
 
 select last_name, first_name, birth, death, timestampdiff(year,birth,death) as age
 from president
 where last_name like 'w%'
 order by last_name;
 
-- 14. display all president's first and last names, birth and death dates (death dates may be NULL), whose names have only four characters, order by last name:  
 select last_name, first_name, birth, death
 from presidnet
 
-- 15. display a list of all the states in which presidents have been born, in ascending order, remove duplicate values 
 select count(*) even
 
 
-- 16. display only the number of members there are  
show tables;
select *from grade_event;

select count(*)
from grade_event
where category='Q';
 
-- 17. display how many quizzes have been given to the class so far 
-- 18. display the number of members, number of email addresses, and number of members w/o lifetime memberships 
-- (NULL values in expiration indicate lifetime memberships) 
 
-- 19. display the number of different states in which presidents have been born, remove duplicate values (similar to #15) 
 
-- 20. display the total number of students 
 
-- 21. display how many men and women there are in one query 
 
-- 22. display the TOP FOUR states having the largest number of presidents born in those states, sorted by the greatest number of presidents born in each of those states. 
 
-- 23. display how many presidents were born in each month of the year, sorted by month 
 
-- 24. display the states, and number of presidents born in each state, with two or more presidents born there, sorted by the greatest number of presidents in each of those states. 
 
-- NOTE: In addition, this query finds duplicate values, to find nonduplicated values, use having count = 1. 
 
-- display the states where only one president was born, sorted by state 
 
-- 25. Display aggregate summaries: 
-- display the minimum, maximum, span (difference between maximum and minimum scores), total, average, and number of scores for each quiz and test taken to date 
