> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


# LIS3781 -Advance Database Management

## Christian T. Ash

### LIS3781 Project 2  Requirements:

1. Installation and import of MongoDB
2. A brief introduction to NoSQL databases
3. Reports that must be generated and JSON code solution

### Project 2 Screenshots:

#### MongoDB Shell Commands

![MongoDB Shell Commands](img/Picture_1.png)

#### Required Reports with JSON Solutions

![JSON Solutionst](img/Picture_2.png)

![JSON Solutionst](img/Picture_3.png)

![JSON Solutionst](img/Picture_4.png)

#### Repo Links:

*Bitbucket Repo - Station Locations:* 

[Bitbucket Repo Link](https://bitbucket.org/ctawork/lis3781/src "Bitbucket Repo Link")
